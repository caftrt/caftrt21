from django.urls import path, include
from . import views 

app_name = "signup"

urlpatterns = [
    path('', views.index, name="index"),
    path('result/', views.new, name="new"),
]