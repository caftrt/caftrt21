from django.db import models
from django.contrib.auth.models import User

# Create your models here.

class Custom(models.Model):
  user = models.OneToOneField(User, null=True, on_delete=models.CASCADE)
  photo = models.ImageField()

  def __str__(self):
    return '{}'.format(self.user.email)