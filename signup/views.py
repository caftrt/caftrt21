from django.shortcuts import render, get_object_or_404
from django.contrib.auth.models import User
from .models import Custom

# Create your views here.

def index(request):
  return render (request, "session/signup.html")

def new(request):
  username = request.POST['username']
  password = request.POST['password']
  email = request.POST['email']
  first_name = request.POST['first_name'] 
  last_name = request.POST['last_name']
  photo = request.POST['photo']

  user = User.objects.create_user(username, email, password)
  user.last_name = last_name
  user.first_name = first_name
  user.save()

  user_custom = get_object_or_404(User, username=username)

  custom = Custom.objects.create()
  custom.user = user_custom
  custom.photo = photo
  custom.save()

  return render(request, "home.html", { 'message': "O usuário ${username} foi criado com sucesso!"})