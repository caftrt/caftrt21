from django.shortcuts import render
from django.contrib.auth import authenticate
from signup.models import Custom


# Create your views here.

def index(request):
  return render (request, "session/login.html")

def new(request):
  username = request.POST['username']
  password = request.POST['password']

  user = authenticate(username=username, password=password)

  custom = Custom.objects.get(user = user)

  return render (request, "dashboard/index.html", {"custom": custom })