from django.shortcuts import render

# Create your views here.

# index --> mostrar a tela de template (login)
# new --> mostrar a tela de template (cadastro) retorna para home
# login --> mostrar a tela de template (só uma tela com o nome do usuário)

def index(request):
  return render(request, "home.html")